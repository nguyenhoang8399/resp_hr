import matplotlib.pyplot as plt

def plot_in_duration(start, end, fs, *data):
    start = int(fs*start)
    end = int(fs*end)
    count =1
    for n in data:
        ax = plt.subplot(len(data), 1, count)
        ax.plot(n[start:end])
        count+=1
    plt.show()
    


