from lib2to3.pgen2.token import N_TOKENS
from re import A
from termios import VMIN
import neurokit2 as nk
import matplotlib.pyplot as plt
from pathlib import Path
from scipy.io import loadmat
import scipy
import pandas as pd
import numpy as np
from scipy.signal import butter
import pickle
from scipy import signal



class SignalProcessing(object):
    def __init__(self, data: dict):
        self.data = data

    def _get_data(self):
        pass

    def windows(self):
        pass

    def filt_by_fft(self):
        pass

    def get_interventions(self):
        pass

    def segment_signal(self, signal, fs, duration):
        nrow = fs*duration
        ncol = int(len(signal)/nrow)+1
        zeros = np.zeros(nrow*ncol)
        for i, _ in enumerate(signal, 0):
            zeros[i] = signal[i]
        return zeros.reshape((nrow, ncol))

    def bandpass_filter(self, order, signal, fs, low, high):
        sos = butter(N=order, Wn=[low, high],
                     btype="bandpass", fs=fs, output='sos')
        filtered = scipy.signal.sosfilt(sos, signal)
        return filtered


class RadarSignalProcessing(SignalProcessing):
    def __init__(self, data: dict, fs: int):
        super().__init__(data)
        self.fs = fs

    def get_radar_i(self):
        return self.data['radar_i']

    def get_radar_data(self):
        return self.data['radar_i'], self.data['radar_q']

    def complex_demodulation(self, radar_i, radar_q) -> np.array:
        def complex_fn(a, b): return complex(a, b)
        assert len(radar_i) == len(radar_q)
        complex_arr = [complex_fn(radar_i[i], radar_q[i])
                       for i in range(len(radar_i))]
        return np.array(complex_arr)

    def _signal_filter_powerline(signal, sampling_rate, powerline=50):
        if sampling_rate >= 100:
            b = np.ones(int(sampling_rate / powerline))
        else:
            b = np.ones(2)
        a = [len(b)]
        y = scipy.signal.filtfilt(b, a, signal, method="pad")
        return y

    @staticmethod
    def remove_dc_offset(radar_i, radar_q):
        return radar_i - radar_i.mean(), radar_q - radar_q.mean()

    def arctangent_signal(self, radar_i, radar_q) -> np.array:
        assert len(radar_i) == len(radar_q)
        # radar_i = np.array([e if np.abs(e) > 0.0000001 else 0.0000001 for e in radar_i ], dtype='float32')
        demodulated_signal = []
        for i in range(len(radar_i)):
            demodulated_signal.append(np.arctan(radar_q[i]/radar_i[i]))
        demodulated_signal = [e*3*pow(10, 8)/(24*pow(10, 9)*4*np.pi)
                              for e in demodulated_signal]
        return np.array(demodulated_signal)

    def _imbalance_factor(self):
        """
        This function's used to find imbalance factor base on paper
        'Data-Based Quadrature Imbalance Compensation
        for a CW Doppler Radar System'
        input: radar_i, radar_q, sampling rate:fs
        output: 
        + alpha: amptitude imbalance
        + phi: angle imbalance
        """
        radar_i = self.data['radar_i']
        radar_q = self.data['radar_q']
        assert len(radar_i) == len(radar_q)
        multified_pair_arr = []
        for i in range(0, len(radar_i)):
            multified_pair_arr.append(radar_i[i]*radar_q[i])

        multified_pair_arr = np.array(multified_pair_arr, dtype='float32')
        square_q_arr = np.array([e*e for e in radar_q], dtype='float32')
        array_one = np.ones(len(radar_i))
        M = np.array([square_q_arr, multified_pair_arr, radar_i,
                     radar_q, array_one], dtype='float32')
        M = M.T
        b = np.array([-e**2 for e in radar_i], dtype='float32')
        b = b.T
        term_1 = M.T@M
        inv_term_1 = np.linalg.inv(term_1)

        coefs = inv_term_1@M.T@b

        imbalance_amp = np.sqrt(1/np.abs(coefs[0]))
        co = coefs[1]/(2*np.sqrt(np.abs(coefs[0])))
        if co < -1:
            co = -1
            print("co: ", co)
        if co > 1:
            co = 1
            print('co: ', co)

        imbalance_phi = np.arcsin(co)

        # print(imbalance_amp, imbalance_phi)
        return imbalance_amp, imbalance_phi

    def correct_i_q(self, w_init=[0, 0], n_iter=200):
        imbalance_amp, imbalance_phi = self._imbalance_factor()
        # matrix_correction = [[imbalance_amp, 0],
        #                      [-np.tan(imbalance_phi)*imbalance_amp, 1/np.cos(imbalance_phi)]]

        matrix_correction = [[1, 0],
                             [-np.tan(imbalance_phi), 1/(imbalance_amp*np.cos(imbalance_phi))]]

        dataset = np.array([self.data['radar_i'], self.data['radar_q']])
        # radius = np.max([np.max(np.abs(self.data['radar_i'])),
        #                 np.max(np.abs(self.data['radar_q']))])

        # radius1 = (np.max(self.data['radar_i']) -
        #            np.min(self.data['radar_i']))/2
        # radius2 = (np.max(self.data['radar_q']) -
        #            np.min(self.data['radar_q']))/2

        # radius = np.max([radius1, radius2])

        # w = GD(w_init, grad, eta=0.5, n_iter=n_iter,
        #        data=dataset, radius=radius)

        # print("DC offset: ", w)

        radar_i = self.data['radar_i'] - self.data['radar_i'].mean()
        radar_q = self.data['radar_q'] - self.data['radar_q'].mean()

        matrix_correction = np.array(matrix_correction, dtype='float32')
        matrix_radar = np.array([radar_i, radar_q], dtype='float32')
        rs = matrix_correction@matrix_radar

        return rs[0], rs[1]

    def phase_ex(self, x, y):
        # x, y = self.correct_i_q() #radar_i and radar_q
        motion_signal = []
        n_samples = len(x)
        for i in range(1, n_samples):
            numerator = x[i]*(y[i] - y[i-1]) - (x[i] - x[i-1])*y[i]
            denominator = x[i]**2 + y[i]**2
            if i == 1:
                motion_signal.append(numerator/denominator)
            motion_signal.append(motion_signal[i-1]+numerator/denominator)
        return np.array(motion_signal)

    @staticmethod
    def enhanced_heart_signal(w_motion) -> list():
        """
            Base on the paper: Phase-Based Methods for Heart Rate Detection Using UWB Impulse Doppler Radar

        Args:
            motion_signal (_type_): _description_
        Returns:
            _type_: _description_
        """
        from numpy import linalg as LA

        N = len(w_motion)
        L = int(N//2)
        Hankel_matrix = np.zeros((N-L, L))
        for i in range(0, N-L-1):
            cut_matrix = w_motion[i:L+i]
            for i2 in range(0, L-1):
                # print(i2)
                Hankel_matrix[i][i2] = cut_matrix[i2]

        # SVD
        # U, S, V = LA.svd(Hankel_matrix)
        # print(V.shape)


class ReferenceSignalProcessing(SignalProcessing):
    def _clean_ecg(self) -> np.array:
        sr = self.data['fs_ecg']
        ecg = self.data['tfm_ecg']
        ecg_cleaned = nk.ecg_clean(ecg, sampling_rate=sr)
        return ecg_cleaned, sr

    def get_z0(self) -> np.array:
        return self.data['tfm_z0']

    def resp_motion(self) -> list:
        data = self.data
        # icg = data['tfm_icg']
        z0 = self.get_z0()
        ecg_cleaned, sr_ecg = self._clean_ecg()
        instant_peaks, rpeaks = nk.ecg_peaks(ecg_cleaned, sampling_rate=sr_ecg)
        rate = nk.ecg_rate(rpeaks, sampling_rate=sr_ecg,
                           desired_length=len(ecg_cleaned))
        edr = nk.ecg_rsp(rate, sampling_rate=sr_ecg, method='charlton2016')
        return ecg_cleaned, edr, z0


def cut_signal(start, end, data, output_file: Path):
    """cut signal: radar_i, radar_q, z0, ecg, icg, intervention
    into good radar signal
    """
    fs_radar = data['fs_radar']
    fs_ecg = data['fs_ecg']
    fs_z0 = data['fs_z0']
    fs_intervention = data['fs_intervention']
    fs_icg = data['fs_icg']

    start_radar = int(start*fs_radar)
    end_radar = int(end*fs_radar)

    start_ecg = int(start*fs_ecg)
    end_ecg = int(end*fs_ecg)

    start_icg = int(start*fs_icg)
    end_icg = int(end*fs_icg)

    start_z0 = int(start*fs_z0)
    end_z0 = int(end*fs_z0)

    start_intervention = int(start*fs_intervention)
    end_intervention = int(end*fs_intervention)

    radar_i = data['radar_i']
    radar_q = data['radar_q']
    tfm_ecg1 = data['tfm_ecg1']
    tfm_ecg2 = data['tfm_ecg2']
    tfm_icg = data['tfm_icg']
    tfm_z0 = data['tfm_z0']
    tfm_intervention = data['tfm_intervention']

    wrap_data = {
        'radar_i': radar_i[start_radar:end_radar],
        'radar_q': radar_q[start_radar:end_radar],
        'tfm_ecg1': tfm_ecg1[start_ecg:end_ecg],
        'tfm_ecg2': tfm_ecg2[start_ecg:end_ecg],
        'tfm_icg': tfm_icg[start_icg:end_icg],
        'tfm_z0': tfm_z0[start_z0:end_z0],
        'tfm_intervention': tfm_intervention[start_intervention:end_intervention]
    }

    with open(output_file.resolve(), 'wb') as f:
        pickle.dump(wrap_data, f)


def plot_in_duration(start, end, fs, *data):
    start = int(fs*start)
    end = int(fs*end)
    count = 1
    for n in data:
        ax = plt.subplot(len(data), 1, count)
        ax.plot(n[start:end], 'r.')
        count += 1
    plt.show()


def resample(origin_data, origin_fs, target_fs):
    len_new_data = int(len(origin_data)*target_fs/origin_fs)+1
    new_data = np.zeros(len_new_data)
    for i, _ in enumerate(origin_data):
        index_of_new_data = int(i*target_fs/origin_fs)
        new_data[index_of_new_data] = origin_data[i]
    return new_data


def cost(w, data, radius):
    rs = []
    for i in range(data.shape[1]):
        err_e = (np.sqrt((data[0][i] - w[0])**2 +
                 (data[1][i] - w[1])**2) - radius)**2
        rs.append(err_e)
    return np.sum(rs)/len(data)


def grad(w, data, radius):
    grad_i_arr = []
    grad_q_arr = []
    n_sample = len(data[0])
    for i in range(data.shape[1]):
        b = np.sqrt((w[0]-data[0][i])**2 + (w[1] - data[1][i])**2)
        term1 = 1-radius/b
        grad_i = term1*(w[0] - data[0][i])
        grad_q = term1*(w[1] - data[1][i])
        grad_i_arr.append(grad_i)
        grad_q_arr.append(grad_q)
    i_derivate = 2*np.sum(grad_i_arr)/n_sample
    q_derivate = 2*np.sum(grad_q_arr)/n_sample
    return np.array([i_derivate, q_derivate])


def GD(w_init, grad, eta, n_iter, data, radius):
    w = [w_init]
    for i in range(n_iter):
        w_new = w[-1]-grad(w[-1], data, radius)*eta
        # print(w_new)
        print(cost(w[-1], data, radius))
        if np.abs(w_new[0] - w[-1][0]) < 1e-5:
            if np.abs(w_new[1] - w[-1][1]) < 1e-5:
                print(i)
                break
        w.append(w_new)
    return w[-1]


def test_gradient():
    infor_file = Path('data/good_signals/info.pkl')
    with open(infor_file.resolve(), 'rb') as info_f:
        info = pickle.load(info_f)
    data_file = Path('data/good_signals/GDN0003/GDN0003_1_Resting_0_300.pkl')
    with open(data_file.resolve(), 'rb') as data_f:
        data = pickle.load(data_f)

    window_len = 30  # seconds
    start_radar = 0*int(info['fs_radar'])
    end_radar = start_radar+window_len*int(info['fs_radar'])

    start_z0 = 0*int(info['fs_z0'])
    end_z0 = start_z0 + window_len*int(info['fs_z0'])

    window_data_i = data['radar_i'][start_radar:end_radar]
    window_data_q = data['radar_q'][start_radar:end_radar]

    dataset = np.array([window_data_i, window_data_q])
    radius = np.max([np.max(np.abs(data['radar_i'])),
                    np.max(np.abs(data['radar_q']))])
    w_init = [0.5, 0.5]
    w_es = GD(w_init, grad, 0.5, 100, dataset, radius)


def applied_windows(data, sr, l_window_in_seconds):
    len_window = sr*l_window_in_seconds
    window = signal.windows.hamming(int(len_window))
    rs = np.convolve(data, window, mode='same')
    return rs

def plot_freq(data, sr):
    from scipy.fft import rfft, rfftfreq
    from scipy.fft import fft, fftfreq
    
    # Number of sample points
    N = len(data)
    # sample spacing
    T = 1.0 / sr
    yf = fft(data)
    xf = fftfreq(len(yf), 1/sr)
    import matplotlib.pyplot as plt
    plt.plot(xf, np.abs(yf), '-b')
    # plt.legend(['FFT'])
    idx = np.argmax(np.abs(yf))
    freq = xf[idx]
    print("Max-power Signal: {} bpm".format(freq*60))
    plt.show()

def fft_filter(data):
    pass

def test_radar_signal_v2():
    # prepare reference data.
    infor_file = Path('data/good_signals/info.pkl')
    with open(infor_file.resolve(), 'rb') as info_f:
        info = pickle.load(info_f)
    data_file = Path('data/good_signals/GDN0005/GDN0005_3_Resting_0_150.pkl')
    with open(data_file.resolve(), 'rb') as data_f:
        data = pickle.load(data_f)

    window_len = 30  # seconds
    start_in_s = 0

    start_radar = start_in_s*int(info['fs_radar'])
    end_radar = start_radar+window_len*int(info['fs_radar'])

    start_z0 = start_in_s*int(info['fs_z0'])
    end_z0 = start_z0 + window_len*int(info['fs_z0'])

    start_ecg = start_in_s*int(info['fs_ecg'])
    end_ecg = start_ecg + window_len*int(info['fs_ecg'])

    window_z0 = data['tfm_z0'][start_z0:end_z0]
    window_ecg1 = data['tfm_ecg1'][start_ecg:end_ecg]

    window_data_i = data['radar_i'][start_radar:end_radar]
    window_data_q = data['radar_q'][start_radar:end_radar]
    
    wrap_window = {
        'radar_i': window_data_i,
        'radar_q': window_data_q
    }

    radar_process = RadarSignalProcessing(wrap_window, int(info['fs_radar']))
    window_i_corr, window_q_corr = radar_process.correct_i_q()

    # arctan_motion = radar_process.arctangent_signal(
    #     window_i_corr, window_q_corr)
    # arctan_ori = radar_process.arctangent_signal(window_data_i, window_data_q)
    motion_signal = radar_process.phase_ex(window_i_corr, window_q_corr)
    # motion_windows = applied_windows(motion_signal, 2000, 0.5)

    radar_process.enhanced_heart_signal(motion_signal)

    # plot_freq(motion_windows, 2000)
    # plot_freq(motion_signal, 2000)
    b, a = scipy.signal.butter(N=3, btype='bandpass', Wn=[
                               0.5, 2], output='ba', fs=2000)
    filter_signal = signal.filtfilt(b, a, motion_signal)

    sos = scipy.signal.butter(N=3, btype='bandpass', Wn=[
                              0.5, 2], output='sos', fs=int(info['fs_ecg']))
    fecg1 = signal.sosfilt(sos, window_ecg1)

    window = signal.windows.hann(int(0.1*2000))
    sos2 = scipy.signal.butter(N=4, btype='lp', Wn=10, output='sos', fs=100)
    
    filter_signal = resample(filter_signal, 2000, 100)
    fecg1 = resample(fecg1,info['fs_ecg'], 100)
    plot_freq(filter_signal, 100)
    plot_freq(fecg1, 100)

    f, t, Sxx = signal.spectrogram(filter_signal, fs=100, window=window)

    ax1 = plt.subplot(511)
    ax1.plot(window_i_corr, window_q_corr)

    ax2 = plt.subplot(512)
    ax2.plot(filter_signal/np.max(np.abs(filter_signal)), 'b+')
    ax2.plot(fecg1/np.max(np.abs(fecg1)), 'r')

    ax3 = plt.subplot(513)
    ax3.plot(motion_signal, 'r+')

    ax4 = plt.subplot(514)
    ax4.pcolormesh(t, f, Sxx, shading='auto')

    ax5 = plt.subplot(515)
    ax5.plot(window_z0)
    plt.show()


if __name__ == "__main__":
    # data_normal_file = Path(
    #     'data/datasets_subject_01_to_10_scidata/GDN0010/GDN0010_3_Apnea.mat')

    # target_fs = 100
    # data_normal = loadmat(data_normal_file)

    # fs_radar = data_normal['fs_radar'][0][0]
    # fs_icg = data_normal['fs_icg'][0][0]
    # fs_ecg = data_normal['fs_ecg'][0][0]
    # fs_z0 = data_normal['fs_z0'][0][0]
    # fs_invention = data_normal['fs_z0'][0][0]

    # tfm_icg = data_normal['tfm_icg']
    # tfm_icg = np.array([e[0] for e in tfm_icg])

    # tfm_z0 = data_normal['tfm_z0']
    # tfm_z0 = np.array([e[0] for e in tfm_z0])

    # radar_i_normal = data_normal['radar_i']
    # radar_i_normal = np.array([e[0]/1000 for e in radar_i_normal])

    # radar_q_normal = data_normal['radar_q']
    # radar_q_normal = np.array([e[0]/1000 for e in radar_q_normal])

    # tfm_icg_normal = data_normal['tfm_icg']
    # tfm_icg_normal = np.array([e[0] for e in tfm_icg_normal])

    # tfm_z0_normal = data_normal['tfm_z0']
    # tfm_z0_normal = np.array([e[0] for e in tfm_z0_normal])

    # tfm_ecg1_normal = data_normal['tfm_ecg1']
    # tfm_ecg1_normal = np.array([e[0] for e in tfm_ecg1_normal])

    # tfm_ecg2_normal = data_normal['tfm_ecg2']
    # tfm_ecg2_normal = np.array([e[0] for e in tfm_ecg2_normal])

    # tfm_intervention_normal = data_normal['tfm_intervention']
    # tfm_intervention_normal = np.array([e[0] for e in tfm_intervention_normal])

    # output_prefix = Path('data/good_signals/GDN0010')
    # file_name = data_normal_file.name.split('.')
    # l_data = len(radar_i_normal)/fs_radar

    # start = 0
    # end = l_data

    # print('len in seconds: ', len(radar_i_normal)/fs_radar)
    # file_name = '{}_{}_{}.pkl'.format(file_name[0], start, int(end))
    # fpath = output_prefix/file_name

    # # inforpath = Path('data/good_signals/info.pkl')
    # # infor = {
    # #     'fs_radar': fs_radar,
    # #     'fs_ecg': fs_ecg,
    # #     'fs_icg': fs_icg,
    # #     'fs_z0': fs_z0,
    # #     'fs_intervention': fs_invention
    # # }

    # # with open(inforpath.resolve(), 'wb') as f:
    # #     pickle.dump(infor, f)

    # data = {
    #     'fs_radar': fs_radar,
    #     'fs_ecg': fs_ecg,
    #     'fs_icg': fs_icg,
    #     'fs_z0': fs_z0,
    #     'fs_intervention': fs_invention,

    #     'radar_i': radar_i_normal,
    #     'radar_q': radar_q_normal,
    #     'tfm_ecg1': tfm_ecg1_normal,
    #     'tfm_ecg2': tfm_ecg2_normal,
    #     'tfm_icg': tfm_icg_normal,
    #     'tfm_z0': tfm_z0_normal,
    #     'tfm_intervention': tfm_intervention_normal

    # }

    # # prepare radar data
    # resampled_radar_i = resample(radar_i_normal, fs_radar, target_fs)
    # resampled_radar_q = resample(radar_q_normal, fs_radar, target_fs)

    # radar_data = {
    #     "radar_i": radar_i_normal,
    #     "radar_q": radar_q_normal
    # }

    # # plot_in_duration(start, end, target_fs, resampled_radar_q, resampled_radar_i, tfm_z0)

    # cut_signal(start, end, data, fpath)
    # with open(fpath, 'rb') as f:
    #     data_f = pickle.load(f)

    test_radar_signal_v2()
