import pandas as pd
from scipy.io import loadmat
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
from signal_processing import Preprocess

data_normal_file = Path('data/datasets_subject_01_to_10_scidata/GDN0004/GDN0004_1_Resting.mat')
data_apnea_file = Path('data/datasets_subject_01_to_10_scidata/GDN0004/GDN0004_3_Apnea.mat')

target_fs = 256

data_apnea = loadmat(data_apnea_file)

fs_radar = int(data_apnea['fs_radar'])
fs_icg = data_apnea['fs_icg']
fs_z0 = data_apnea['fs_z0']

radar_i = np.asanyarray(data_apnea['radar_i'])
radar_q = np.asanyarray(data_apnea['radar_q'])

tfm_icg = data_apnea['tfm_icg']
tfm_z0 = data_apnea['tfm_z0']

data_normal = loadmat(data_normal_file)
radar_i_normal = np.asarray(data_normal['radar_i'])
radar_q_normal = np.asarray(data_normal['radar_q'])

tfm_icg_normal= data_normal['tfm_icg']
tfm_z0_normal= data_normal['tfm_z0']


def resample(origin_data, origin_fs, target_fs):
    len_new_data = int(len(origin_data)*target_fs/origin_fs)+1
    new_data = np.zeros(len_new_data)
    for i, _ in enumerate(origin_data):
        index_of_new_data = int(i*target_fs/origin_fs)
        new_data[index_of_new_data] = origin_data[i]
    return new_data
    
def plot_data(signal):
    plt.plot(signal)
    plt.show()
    
def handle_ref_data(signal):
    pass

def test():
    start_in_seconds = 0
    end_in_seconds = 60
    
    resampled_radar_i = resample(radar_i_normal, fs_radar, target_fs)
    resampled_radar_q = resample(radar_q_normal, fs_radar, target_fs)

    ax1 = plt.subplot(311)
    time_ax1 = np.arange(0, end_in_seconds - start_in_seconds, 1/fs_radar)
    ax1.plot(time_ax1, resampled_radar_q[start_in_seconds*fs_radar:end_in_seconds*fs_radar])
    
    ax2 = plt.subplot(312)
    time_ax2 = np.arange(0, end_in_seconds - start_in_seconds, 1/256)
    ax2.plot(time_ax2, resampled_radar_i[start_in_seconds*256:end_in_seconds*256])
    
    data = {
        "radar_i": resampled_radar_i,
        "radar_q": resampled_radar_q,
        "fs_radar": target_fs
    }

    preprocess = Preprocess(data)
    arctan = preprocess.actangent_signal()

    ax3 = plt.subplot(313)
    time_ax3 = np.arange(0, end_in_seconds - start_in_seconds, 1/256)
    ax3.plot(time_ax3, arctan[start_in_seconds*256:end_in_seconds*256])
    plt.show()
   
    # ax1 = plt.subplot(311)
    # time_ax1 = np.arange(0, len(radar_i_normal)/fs_radar, 1/fs_radar)
    
    # ax1.plot(time_ax1, radar_i_normal)
    
    # ax2 = plt.subplot(312)
    # time_ax2 = np.arange(0, len(tfm_z0_normal)/fs_z0, 1/fs_z0)
    # ax2.plot(time_ax2, tfm_z0_normal)
    
    # ax3 = plt.subplot(313)
    # time_ax3 = np.arange(0, len(tfm_icg_normal)/fs_icg, 1/fs_icg)
    # ax3.plot(time_ax3, tfm_icg_normal)
    # plt.show()
    
    
if __name__ == "__main__":
    test()





